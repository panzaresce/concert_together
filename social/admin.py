from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.admin import AdminSite

from social.models import UserProfile, Artist, UserGroup, Event, Group, Performance, Message


class AppAdminSite(AdminSite):
    site_header = "Concert Together Admin"
    index_title = "App Models"
    login_template = 'registration/login.html'
    list_filter = ('Group',)


admin_site = AppAdminSite(name='adminapp')


class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'email')
    search_fields = ('username',)


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'short_bio', 'profile_photo')


class UserGroupAdmin(admin.ModelAdmin):
    list_display = ('user', 'is_owner', 'needs_ride', 'entry_date', 'group')
    list_filter = ('user',)


class EventAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    list_display = ('id', 'name', 'date', 'location')
    list_filter = ('date',)


class GroupAdmin(admin.ModelAdmin):
    list_display = ('id', 'group_name', 'event', 'max_n_seat')
    search_fields = ('group_name',)


class PerformanceAdmin(admin.ModelAdmin):
    list_display = ('artist', 'event')
    list_filter = ('artist',)


class MessageAdmin(admin.ModelAdmin):
    list_display = ('text', 'user_group')
    list_filter = ('user_group',)


# Register your models here.
admin_site.register(User, UserAdmin)
admin_site.register(UserProfile, UserProfileAdmin)
admin_site.register(Artist)
admin_site.register(UserGroup, UserGroupAdmin)
admin_site.register(Event, EventAdmin)
admin_site.register(Group, GroupAdmin)
admin_site.register(Performance, PerformanceAdmin)
admin_site.register(Message, MessageAdmin)
