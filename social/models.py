from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='user_profile', on_delete=models.CASCADE)
    short_bio = models.TextField(null=True, max_length=255, default='I love music !!!')
    profile_photo = models.ImageField(null=True, upload_to='profile_photo', default='default_user.png')


class Artist(models.Model):
    name = models.CharField(max_length=30, unique=True, help_text="The name of the artist or band")

    def __str__(self):
        return f'{self.name}'


class Event(models.Model):
    name = models.CharField(max_length=30, help_text="The name of the event")
    date = models.DateField(help_text="The date of the event")
    location = models.CharField(max_length=50, help_text="The location of the event")
    artist = models.ManyToManyField(Artist, through="Performance")

    def __str__(self):
        return f'{self.name}, {self.date}, {self.location}, {self.artist.all()}'


class Performance(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)


class Group(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    group_name = models.CharField(max_length=30)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    user = models.ManyToManyField(User, through="UserGroup")
    max_n_seat = models.PositiveSmallIntegerField(default=5)

    def __str__(self):
        return f'{self.id}, {self.create_date}, {self.group_name}, {self.event}, {self.user.all()}'


class UserGroup(models.Model):
    entry_date = models.DateTimeField(auto_now_add=True, help_text="Timestamp when the user enter the group")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    is_owner = models.BooleanField(default=False)
    needs_ride = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.id}, {self.user.username}, {self.group.group_name}'


class Message(models.Model):
    text = models.CharField(max_length=255, help_text="The text of the message")
    send_tmstmp = models.DateTimeField(auto_now_add=True)
    user_group = models.ForeignKey(UserGroup, on_delete=models.CASCADE)
