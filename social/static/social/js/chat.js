function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            url: props.url,
            csrftoken: getCookie('csrftoken'),
            check_inteval: props.seconds,
            divHeight: 0,
            messages: []
        }
        this.fetchData()
    }

    componentDidMount() {
        this.intervalId = setInterval(() => {
            this.fetchData();
        }, this.state.check_inteval)
    }

    componentDidUpdate() {
        if (this.state.divHeight < $("#animation").parent().prop("scrollHeight")) {
            this.setState({divHeight: $("#animation").parent().prop("scrollHeight")})
            $("#animation").parent().animate({scrollTop: $("#animation").parent().prop("scrollHeight")}, 500);
        }

    }

    fetchData() {
        fetch(this.state.url, {
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRFToken': this.state.csrftoken,
            },
        }).then(response => {
            return response.json()
        }).then(data => {
            this.setState({messages: data, divHeight: $("#animation").parent().prop("scrollHeight")})
        })
    }
    
    render() {
        var msgList = this.state.messages.map((msg) =>
            <div class="container card msg-container">
                <div class="row card-header msg-username">
                    <p>{msg.user_group.user.username}</p>
                </div>
                <div class="row msg-text">
                    <p>{msg.text}</p>
                </div>
                <footer class="msg-tmstmp">
                    <p>{msg.send_tmstmp}</p>
                </footer>
            </div>
        )
        return (
            <div class="container row gy-3" id="animation">{msgList}</div>
        );
    }
}