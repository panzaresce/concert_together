from social.models import Message
from rest_framework import serializers


class GroupMessageSerializer(serializers.ModelSerializer):
    send_tmstmp = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    class Meta:
        model = Message
        fields = ['text', 'send_tmstmp', 'user_group']
        depth = 2
