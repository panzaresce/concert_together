from django.core.management.base import BaseCommand
from social.models import Artist


class Command(BaseCommand):
    help = 'Populate the artist table.'

    def handle(self, *args, **options):
        Artist(name='Linkin Park').save()
        Artist(name='Metallica').save()
        Artist(name='Alter Bridge').save()
        Artist(name='AC/DC').save()
        Artist(name='Nanowar of Steel').save()
        Artist(name='Slipknot').save()
        Artist(name='Sum 41').save()
        Artist(name='S.O.A.D.').save()
        Artist(name='Megadeth').save()
        Artist(name='Iron Maiden').save()
        Artist(name='Meshuggah').save()
        Artist(name='Dream Theater').save()
        Artist(name='DragonForce').save()
        Artist(name='Black Sabbath').save()
        Artist(name='Pantera').save()

