from unittest.mock import Mock
from django.test import TestCase, Client, RequestFactory
from datetime import date
from django.contrib.auth.models import User
from social.models import Group, Event, Artist


class TestUserFlow(TestCase):
    """Test all the user flow, from login to interactions with groups (enter, exit, delete, ...)"""

    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.test_user1 = User.objects.create_user(username="testuser1", password="PasswordTest@123QWERTY")
        self.test_user2 = User.objects.create_user(username="testuser2", password="PasswordTest@123QWERTY")
        self.test_user1.save()
        self.test_user2.save()

        # Fill artist
        self.a1 = Artist.objects.create(name="Artist1")
        self.a2 = Artist.objects.create(name="Artist2")
        self.a3 = Artist.objects.create(name="Artist3")

        # Create events
        self.e1 = Event.objects.create(name="Event1", date=date.today(), location="City")
        self.e1.save()
        self.e1.artist.set([self.a1, self.a2])

        self.e2 = Event.objects.create(name="Event2", date=date.today(), location="Another City")
        self.e2.save()
        self.e2.artist.set([self.a2, self.a3])

        self.e3 = Event.objects.create(name="Event3", date=date.today(), location="Big City")
        self.e3.save()
        self.e3.artist.set([self.a1, self.a3])

        self.e4 = Event.objects.create(name="Event4", date=date.today(), location="Small City")
        self.e4.save()
        self.e4.artist.set([self.a2, self.a3])

        # Create groups
        self.g1 = Group.objects.create(group_name="Group1", event=self.e1)
        self.g1.save()
        self.g1.user.set([self.test_user1], through_defaults={'is_owner': True})
        self.g1.user.set([self.test_user2])

        self.g2 = Group.objects.create(group_name="Group2", event=self.e2)
        self.g2.save()
        self.g2.user.set([self.test_user1], through_defaults={'is_owner': True})

        self.g3 = Group.objects.create(group_name="Group3", event=self.e3)
        self.g3.save()
        self.g3.user.set([self.test_user1], through_defaults={'is_owner': True})
        self.g3.user.set([self.test_user2])

        self.g4 = Group.objects.create(group_name="Group4", event=self.e4)
        self.g4.save()
        self.g4.user.set([self.test_user1], through_defaults={'is_owner': True})

    def test_anon_user_redirect(self):
        response = self.client.get('/enter')
        self.assertEqual(response.status_code, 302)

    def test_user_login(self):
        login = self.client.login(username='testuser1', password='PasswordTest@123QWERTY')
        response = self.client.get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_enter_group(self):
        login = self.client.login(username='testuser2', password='PasswordTest@123QWERTY')
        response = self.client.post('/enter', data={'enter-form-pre': '',
                                                    'enter-form-pre-needs_ride': 'on',
                                                    'enter-form-pre-group_id': f'{self.g2.id}'})

        self.assertIsNotNone(Group.objects.get(id=self.g2.id).user.get(id=self.test_user2.id))

    def test_exit_group(self):
        grop_n_before = User.objects.get(id=self.test_user2.id).group_set.count()
        login = self.client.login(username='testuser2', password='PasswordTest@123QWERTY')
        response = self.client.get(f'/exit/{self.g1.id}')
        grop_n_after = User.objects.get(id=self.test_user2.id).group_set.count()
        self.assertGreater(grop_n_before, grop_n_after)

    def test_delete_group_if_owner(self):
        login = self.client.login(username='testuser1', password='PasswordTest@123QWERTY')
        response = self.client.get(f'/delete/{self.g2.id}')
        self.assertNotIn(self.g2, Group.objects.all())

    def test_delete_group_if_not_owner(self):
        login = self.client.login(username='testuser2', password='PasswordTest@123QWERTY')
        response = self.client.get(f'/delete/{self.g3.id}')
        self.assertIn(self.g3, Group.objects.all())

    def test_delete_group_if_not_in_group(self):
        login = self.client.login(username='testuser2', password='PasswordTest@123QWERTY')
        response = self.client.get(f'/delete/{self.g4.id}')
        self.assertIn(self.g4, Group.objects.all())
