import time, datetime
from django import forms
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError

from social.models import UserProfile, Event, Artist, UserGroup


class SearchForm(forms.Form):
    FILTER_CHOICES = (('artist', 'Artista'), ('location', 'Luogo'), ('date', 'Data'))

    content = forms.CharField(max_length=20,
                              widget=forms.TextInput(attrs={'placeholder': 'Cerca (data Y-M-D)', 'class': 'form-control'}))
    filters = forms.ChoiceField(choices=FILTER_CHOICES,
                                widget=forms.Select(attrs={'class': 'form-select form-select-sm'}))


class RegisterForm(UserCreationForm):

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2', 'email']


class AuthorProfileForm(forms.ModelForm):
    username = forms.CharField(max_length=255)
    email = forms.EmailField()

    class Meta:
        model = UserProfile
        fields = ['username', 'email', 'short_bio', 'profile_photo']

    def clean_profile_photo(self):
        upload_dir = UserProfile._meta.get_field('profile_photo').upload_to
        default_img = UserProfile._meta.get_field('profile_photo').default
        img = self.cleaned_data['profile_photo']
        if upload_dir not in img.name and default_img not in img.name:
            img.name = str(int(time.time())) + "_" + img.name
        return img


class AddEventForm(forms.ModelForm):
    artist = forms.ModelMultipleChoiceField(queryset=Artist.objects.all(),
                                            widget=forms.CheckboxSelectMultiple)
    n_seat = forms.IntegerField(min_value=0)

    class Meta:
        model = Event
        fields = ['name', 'date', 'location', 'artist']

    def clean_date(self):
        if self.cleaned_data['date'] <= datetime.date.today():
            raise ValidationError('L\' evento non può avvenire oggi o nel passato')
        else:
            return self.cleaned_data['date']


class EnterGroupForm(forms.ModelForm):
    group_id = forms.IntegerField(widget=forms.HiddenInput)

    class Meta:
        model = UserGroup
        fields = ['needs_ride']


def form_validation_error(request, form):
    msg = ""
    for field in form:
        for error in field.errors:
            msg = "%s: %s \n" % (field.label if hasattr(field, 'label') else 'Error', error)
            messages.error(request, msg)
    return msg
