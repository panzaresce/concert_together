from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from social.views import ProfileView, UserCreationView, ConcertView, ChatView, enter_group_view, exit_group_view, \
    delete_group_view
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('register/', UserCreationView.as_view(), name='register'),
    path('profile/', ProfileView.as_view(), name='profile'),
    path('concerti/', ConcertView.as_view(), name='concerts'),
    path('enter', enter_group_view, name='enter-group'),
    path('chat/<int:group_key>', ChatView.as_view(), name='chat-group'),
    path('exit/<int:group_key>', exit_group_view, name='exit-group'),
    path('delete/<int:group_key>', delete_group_view, name='delete-group'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
