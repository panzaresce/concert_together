from django.contrib.auth.decorators import login_required
from social.serializers import GroupMessageSerializer
from datetime import date, datetime
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from django.template.defaultfilters import register
from django.urls import reverse_lazy
from django.views.generic import View, CreateView, TemplateView
from social.forms import SearchForm, AuthorProfileForm, RegisterForm, form_validation_error, AddEventForm, \
    EnterGroupForm
from social.models import UserProfile, Artist, Group, UserGroup, Message


class Homepage(TemplateView):
    template_name = 'home.html'
    success_url = reverse_lazy('home')

    def get(self, request, *args, **kwargs):

        groups = _get_groups()

        return self.render_to_response({'search_form': SearchForm(prefix='search-form-pre'),
                                        'event_form': AddEventForm(prefix='event-form-pre'),
                                        'enter_group_form': EnterGroupForm(prefix='enter-form-pre'),
                                        'groups': groups})

    def post(self, request, *args, **kwargs):
        search_form = _get_form(request, SearchForm, 'search-form-pre')
        event_form = _get_form(request, AddEventForm, 'event-form-pre')
        if search_form.is_bound and search_form.is_valid():
            keyword = search_form.cleaned_data.get('content')
            filter_choice = search_form.cleaned_data.get('filters')
            if filter_choice == 'artist':
                groups = Group.objects.all().order_by('event__date').filter(event__artist__name__icontains=keyword)
            elif filter_choice == 'location':
                groups = Group.objects.all().order_by('event__date').filter(event__location__icontains=keyword)
            elif filter_choice == 'date':
                groups = Group.objects.all().order_by('event__date').filter(
                    event__date__range=[date.today(), datetime.strptime(keyword, "%Y-%m-%d")])

            groups = _get_groups(groups)

            return self.render_to_response({'search_form': SearchForm(prefix='search-form-pre'),
                                            'event_form': AddEventForm(prefix='event-form-pre'),
                                            'enter_group_form': EnterGroupForm(prefix='enter-form-pre'),
                                            'groups': groups})
        else:
            form_validation_error(request, search_form)
        if event_form.is_bound and event_form.is_valid():
            # Create Event
            new_event = event_form.save()
            artist_list = event_form.cleaned_data.get('artist')
            for artist in artist_list:
                new_event.artist.add(Artist.objects.get(name=artist))

            # Create Group
            group_name = event_form.cleaned_data.get('name')
            n_seat = event_form.cleaned_data.get('n_seat')
            group = Group.objects.create(group_name=group_name, event=new_event, max_n_seat=n_seat)
            group.user.set([request.user], through_defaults={'is_owner': True})

            messages.success(request, 'Event added successfully')
        else:
            form_validation_error(request, event_form)
        return redirect('home')


class UserCreationView(CreateView):
    form_class = RegisterForm
    template_name = 'registration/registration.html'
    success_url = reverse_lazy('home')

    def post(self, request, *args, **kwargs):
        form = RegisterForm(request.POST)
        if form.is_valid():
            valid = super(UserCreationView, self).form_valid(form)
            username, password = form.cleaned_data.get('username'), form.cleaned_data.get('password1')
            new_user = authenticate(username=username, password=password)
            UserProfile.objects.create(user=new_user)
            login(self.request, new_user)
            return valid
        form_validation_error(request, form)
        return redirect('register')


class ProfileView(LoginRequiredMixin, View):
    profile = None

    def dispatch(self, request, *args, **kwargs):
        self.profile, __ = UserProfile.objects.get_or_create(user=request.user)
        return super(ProfileView, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        context = {'profile': self.profile}
        return render(request, 'social/profile.html', context)

    def post(self, request):
        form = AuthorProfileForm(request.POST, request.FILES, instance=self.profile)

        if form.is_valid():
            form.save()
            messages.success(request, 'Profile saved successfully')
        else:
            form_validation_error(request, form)
        return redirect('profile')


class ConcertView(LoginRequiredMixin, TemplateView):
    template_name = 'social/concert.html'
    success_url = reverse_lazy('concerts')

    def get(self, request, *args, **kwargs):
        groups = Group.objects.all().filter(user=request.user).order_by('-event__date')

        groups = _get_groups(groups)

        return self.render_to_response({'groups': groups})


class ChatView(LoginRequiredMixin, TemplateView):
    template_name = 'social/chat.html'

    def get(self, request, *args, **kwargs):
        group_id = kwargs['group_key']
        group, __ = self._get_chat(group_id)
        # return self.render_to_response({'group': group, 'messages': group_messages, 'task_id': res.id})
        group = _get_groups(group)
        return self.render_to_response({'group': group})

    def post(self, request, **kwargs):
        body = request.body.decode("utf-8")
        group_id = kwargs['group_key']
        if not body:
            group, group_messages = self._get_chat(group_id)
            ser = GroupMessageSerializer(group_messages, many=True).data
            return JsonResponse(ser, safe=False)
        else:
            group = Group.objects.get(id=group_id)
            if request.user in group.user.all():
                ug = UserGroup.objects.get(user=request.user, group=group)
                msg = Message.objects.create(text=body, user_group=ug)
                return HttpResponse(status=200)
            else:
                return redirect('home')

    def _get_chat(self, group_id):
        group = Group.objects.get(id=group_id)
        user_in_group = UserGroup.objects.all().filter(group=group)
        group_messages = Message.objects.all().filter(user_group__in=user_in_group).order_by('send_tmstmp')
        return group, group_messages


@login_required
def enter_group_view(request):
    enter_group_form = _get_form(request, EnterGroupForm, 'enter-form-pre')

    if enter_group_form.is_valid():

        group_key = enter_group_form.cleaned_data.get('group_id')
        group = Group.objects.get(id=group_key)
        needs_ride = enter_group_form.cleaned_data.get('needs_ride')

        if needs_ride:
            actual_seat = UserGroup.objects.filter(needs_ride=True).filter(group=group).count()
            if actual_seat == group.max_n_seat:
                messages.error(request, "Questo gruppo ha esaurito i posti liberi")
                return redirect('home')

        UserGroup.objects.create(user=request.user, group=group, needs_ride=needs_ride)
        messages.success(request, 'You entered the group')
    else:
        form_validation_error(request, enter_group_form)
    return redirect('home')


@login_required
def exit_group_view(request, group_key):
    group = Group.objects.get(id=group_key)
    UserGroup.objects.get(user=request.user, group=group).delete()
    messages.success(request, 'You exited from the group')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required
def delete_group_view(request, group_key):
    group = Group.objects.get(id=group_key)

    if group not in Group.objects.all().filter(user=request.user) or not UserGroup.objects.get(user=request.user,
                                                                                               group=group).is_owner:
        messages.error(request, 'Can\'t delete group if you are not the owner')
    else:
        group.delete()
        messages.success(request, 'Group deleted successfully')

    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def _get_groups(groups=None):
    if groups is None:
        groups = Group.objects.all().filter(event__date__gte=date.today()).order_by('event__date')

    # check if it is a list of groups or just one group
    if hasattr(groups, '__iter__'):
        for g in groups:
            g.owner_id = UserGroup.objects.filter(is_owner=True).get(group=g).user_id
            g.seats = UserGroup.objects.filter(needs_ride=True).filter(group=g).count()
            g.tot_ppl = UserGroup.objects.filter(group=g).count()
            g.is_past = (True if date.today() > g.event.date else False)
    else:
        groups.owner_id = UserGroup.objects.filter(is_owner=True).get(group=groups).user_id
        groups.seats = UserGroup.objects.filter(needs_ride=True).filter(group=groups).count()
        groups.tot_ppl = UserGroup.objects.filter(group=groups).count()
        groups.is_past = (True if date.today() > groups.event.date else False)
    return groups


def _get_form(request, formcls, prefix):
    data = request.POST if prefix in request.POST else None
    return formcls(data, prefix=prefix)


@register.filter(name="is_in")
def is_in(user, group):
    if user in group.user.all():
        return True
    else:
        return False
