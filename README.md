# UNIse - search engine
Prototype of a concert sharing web app

## Setup
How to run:

```
$ cd concert_together
$ pipenv shell
$ pipenv install
$ python manage.py migrate
$ python manage.py populate_artist
$ python manage.py runserver
```
Once the server start go to <i>localhost:8000</i> from your browser.

## Additional Info
If you want to create the superuser run:

```
$ python manage.py createsuperuser
```
